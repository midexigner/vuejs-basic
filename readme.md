What is Vue.js?

- A front-end framework
- Create javascript drive web application
- Runs in the browser
- No need to make multiple server request for pages.
- Very lean (16kb)
- Very high run-time performace

How to install Vue.js the easy way

- Conditionals
- Events
- Data

How Vue.js with the vue-cli and webpack

- Components
- Vue files & templates

# Vue.js Guide

- https://vuejs.org/v2/guide/index.html

# The Vue CLi (cli = command line interface)
- Create a dev environment workflow with webpack
   - Use ES6 featured
   - Compile & minify our JS into 1 file
   - Use single file templates
   - Compile everything on our machine, not in a browser
   - live reload dev server
   - install node.js
   - install cmder

# Vue.js cli install guide
- https://cli.vuejs.org/
- npm install -g vue-cli
- vue init <template-name> <project-name>
- vue init webpack-simple  my-project
- npm install
- npm run dev

# Editor
- atom
- atom-live-server 

- npm install -g @vue/cli
- vue create appName
- cd appName
- yarn serve

# With npm
npm i vue bootstrap-vue bootstrap

# With yarn
yarn add vue bootstrap-vue bootstrap
yarn add axios

#vue life cycles
![](component-lifecycle.png)
<img src='' align='left' width='50%'>