import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import {public_key } from './marvel';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    characters: [],
    character: [],
    imgURL:'',
    imgExt:'',
    imgSize:'standard_large',  //https://developer.marvel.com/documentation/
  },
  mutations: {
    getCharacters(state)
    {
      state.characters = []
      axios.get(`http://gateway.marvel.com/v1/public/characters?apikey=${public_key}`)
.then((result) => {
   // console.log(result);
    result.data.data.results.forEach((item) =>{
        //console.log(item);
        state.characters.push(item);
    })
})
.catch((error) => {
   console.log(error); 
})
    },
    getCharacter(state,id){
      state.character = []
      axios.get(`http://gateway.marvel.com/v1/public/characters/${id}?apikey=${public_key}`).then((result) => {
   //console.log(result);
    result.data.data.results.forEach((item) =>{
      console.log(item);
      state.character.push(item);
      //state.imgURL = `${item.thumbnail.path}/${state.imgSize}`;
      state.imgURL = `${item.thumbnail.path}`;
      state.imgExt = item.thumbnail.extension;
   }) 
})
.catch((error) => {
  console.log(error); 
})
    }
  },
  actions: {
getCharacters:context =>{
  context.commit('getCharacters')
},
getCharacter(context,id){
  context.commit('getCharacter',id)
}


  }
})
