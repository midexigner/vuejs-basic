-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2019 at 11:22 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopping`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` bigint(20) NOT NULL,
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `brand`) VALUES
(3, 'Polo'),
(6, 'Sketchers'),
(7, 'Helly Hanson'),
(8, 'women'),
(12, 'Piece'),
(15, 'Chiffon'),
(17, 'test'),
(18, 'Charter Club');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` bigint(11) NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` datetime NOT NULL,
  `paid` tinyint(4) NOT NULL DEFAULT '0',
  `shipped` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `parent`) VALUES
(1, 'Men', 0),
(2, 'Women', 0),
(3, 'Boys', 0),
(4, 'Girls', 0),
(5, 'Shirts', 1),
(6, 'Pants', 1),
(7, 'Shoes', 1),
(8, 'Accessories', 1),
(9, 'Shirts', 2),
(10, 'Pants', 2),
(11, 'Shoes', 2),
(12, 'Dresses', 2),
(13, 'Shirts', 3),
(14, 'Pants', 3),
(15, 'Dresses', 4),
(16, 'Shoes', 4),
(17, 'Accessories', 2),
(24, 'Home Decore', 23),
(25, 'Skirts', 2),
(26, 'Pants', 4),
(27, 'Shoes', 3),
(28, 'Onesies', 3);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `list_price` decimal(10,2) NOT NULL,
  `brand` bigint(20) NOT NULL,
  `categories` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(4) NOT NULL DEFAULT '0',
  `sizes` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `price`, `list_price`, `brand`, `categories`, `image`, `description`, `featured`, `sizes`, `deleted`) VALUES
(5, 'High Heels', '99.99', '109.00', 14, '11', 'assets/images/products/66a3137ad994c0dea7903ae770c36e25.jpg', 'wow You will be so tall and beautiful when you wear your new pair!', 1, 's:0:2,m:7:2,l:10:2', 0),
(6, 'Serious Elegant Shoes', '45.99', '65.99', 10, '11', 'assets/images/products/567c60dc8da9febb17a454ce15408b70.png', 'These are perfect for business or dates\r\nYou will love yourself for buying these.\r\nNo series collector of shoes leaves these behind!', 1, 's:5:2,m:5:2,l:5:2', 0),
(7, 'Blouse', '9.99', '19.99', 12, '9', 'assets/images/products/13bcd38db3ba38f44df3bb8325044bcb.png', 'This sleeveless beauty will have you cool and breezy whilst riding in your BMW Convertible on the way to book club!', 1, 's:5:,m:88:,l:4:', 0),
(8, 'Average Purse', '55.99', '74.99', 6, '17', 'assets/images/products/c137b40d4961591b26bbee66678ff0f5.png', 'This is your everyday purse...\r\nYou can tell I&#039;m not women and I know nothing about fashion or clothes.\r\nSo why Did I Make a Clothing Store?', 1, 's:44:2', 0),
(9, 'Long Skirt', '19.99', '39.99', 10, '25', 'assets/images/products/8673b37ca97e03544edb30ebc82a52e1.png', 'What a beautiful Skirt.\r\nYou&#039;ll be turning heads while maintaining integrity!\r\nLook beautiful and feel good about it.', 1, 'm:55:2', 0),
(10, 'Striped Hoodies', '7.99', '15.99', 8, '13', 'assets/images/products/f29b847f6fa8bb90dd8b15e48f7e5a26.png', 'This blue and white hoodie will make any toddler feel like a grown up. This is fashionable and warm.', 1, 's:2:2,m:5:2,l:6:2', 0),
(11, 'Modern Pants', '29.99', '49.99', 1, '14', 'assets/images/products/0a40947e53ebc34a981a615a9487a9f9.png', 'dddd', 1, 's:5:2,m:4:2,l:5:2', 0),
(12, 'Cover Alls', '39.99', '45.99', 3, '14', 'assets/images/products/1ca4289fd751b0a5d973e5f424779e8d.png', 'qq', 1, 's:8:2,m:10:2,L:6:2', 0),
(13, 'Princess Dress', '99.99', '115.00', 7, '15', 'assets/images/products/491c3d2390d9eea3f7ee6b7a2d7d0edb.png', 'Wow! Stunning Make her feel like the perfect, beautiful woman to be that she is!', 1, 's:8:2,m:7:2,l:7:2', 0),
(14, 'Pink and Black Bird', '15.99', '23.99', 2, '15', 'assets/images/products/9c238d50da21897a6d133c22ee08c25a.png', 'www', 1, 's:3:2,m:5:2', 0),
(15, 'Car and Trucks Onesies', '15.99', '25.99', 3, '28', 'assets/images/products/44c8fef5e84295fa0d94610fa41d1b08.png', 's', 1, 's:10:2', 0),
(16, 'Summer Dress', '39.99', '59.99', 15, '12', 'assets/images/products/94d380870d4798723ed95e9365534137.png', 'This heavenly dress is meant to be cherished. Designed from a flowing chiffon and accented with floral lace back yoke. The look is complete with an openwork crochet waistband and bell sleeves.', 1, 'M:2:2,L:22:2', 0),
(17, 'Levis Jeans', '45.99', '55.99', 1, '6', 'assets/images/products/4a61c14ae6eb6f18f8d5045a37717daf.png', 'Look at these beautiful jeans.', 1, '28:3:2,30:4:2,32:1:2,36:2:2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) NOT NULL,
  `charge_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cart_id` bigint(20) NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `grand_total` decimal(10,2) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `txn_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `txn_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(11) NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `join_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime DEFAULT NULL,
  `permissions` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`, `join_date`, `last_login`, `permissions`) VALUES
(1, 'Muhammad Idrees', 'hello@midexigner.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '2017-03-16 22:07:11', '2018-12-25 01:12:27', 'admin,editor'),
(2, 'Test Testerson', 'test@testerson.com', '$2y$10$UvJbppu6JMwoDKQiG/5O.OhrB6KVoTpguwZ5v4vshSYMzIHKKedgO', '2017-03-26 15:00:42', '2017-08-03 20:27:40', 'editor');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
