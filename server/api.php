<?php 
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
$db = new mysqli('localhost','root','','vuejs');
if($db->connect_error){
    die('Could not connect to database!');
}
$res = array('error'=>false);
$action = 'read';
if(isset($_REQUEST['action'])){
    $action = $_REQUEST['action'];
}
if($action == 'read'){
    $result = $db->query("SELECT * FROM users");
    $users = array();
    
while($row= $result->fetch_assoc()){
 array_push($users, $row);
    }
    $res['users'] = $users;
}

if($action == 'create'){
	$username = $_POST['username'];
	$email = $_POST['email'];
	$mobile = $_POST['phone'];
	$result = $db->query("INSERT INTO `users` (`username`, `email`, `phone`) VALUES ('$username', '$email', '$mobile') ");
	
	if($result){
		$res['message'] = "User added successfully";
	} else{
		$res['error'] = true;
		$res['message'] = "Could not insert user";
	}
}

if($action == 'update'){
	$id = $_POST['id'];
	$username = $_POST['username'];
	$email = $_POST['email'];
	$mobile = $_POST['mobile'];

	$result = $db->query("UPDATE `users` SET `username` = '$username', `email` = '$email', `mobile` = '$mobile' WHERE `id` = '$id'");
	
	if($result){
		$res['message'] = "User updated successfully";
	} else{
		$res['error'] = true;
		$res['message'] = "Could not update user";
	}

}

if($action == 'delete'){
	$id = (isset($_POST['id']) ? $_POST['id']:'0');
	$result = $db->query("DELETE FROM `users` WHERE `id` = '$id'");
	
	 if($result > '0'){
		$res['message'] = "User deleted successfully";
	 } else{
		$res['error'] = true;
	 	$res['message'] = "Could not delete user";
	}

}


$db->close();

echo json_encode($res);
die();
?>