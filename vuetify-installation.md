# Setting Up Vuetify
- vue create todo-list
    - Manually select fetures (Babel, Router,Vues,Linter)
    - Use history mode for router? (yes)
    - ESLint with error prevention only
    - Lint on save
    - in package.json
    - Save this as a preset for future (no)
- vue add vuetify
 - default

# Vuetify Basics
- open `app.vue`
```
<v-app>
    <v-content>
     <router-view></router-view>
    </v-content>
  </v-app>
```
### text color Class
```<p class="red white--text">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
  <p class="pink lighten-4 red--text">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
  <p class="indigo lighten-4 indigo--text">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
   <h1 class="display-4">Massive SiDisplayze</h1>
   <h3 class="display-1">Small Display</h3>
   <p class="headline">This is a headline</p>
    <p class="title font-weight-bold">This is a subheading</p>
     <p class="caption">This is a caption</p>
```
### Button & Icons
```
<v-btn class="pink white--text">Button</v-btn>
     <v-btn dark depressed color="pink">Click me</v-btn>
      <v-btn text color="pink" disabled>Click me2</v-btn>
      <v-btn class="pink white--text"><v-icon left>mdi-email</v-icon><span>Email Me</span></v-btn>
      <v-btn class="pink white--text" small><v-icon left small>mdi-email</v-icon><span>Email Me</span></v-btn>
       <v-btn class="pink white--text" large><span>Email Me</span><v-icon right large>mdi-email</v-icon></v-btn>

       <v-btn class="pink white--text" small><v-icon left small>mdi-email</v-icon><span>Email Me</span></v-btn>
       <v-btn fab depressed small dark color="purple" ><v-icon >mdi-heart</v-icon></v-btn>
 ```
### Breakpoints & Visibility

```
 <v-btn fab depressed small dark color="purple" class="hidden-md-and-down" ><v-icon >mdi-heart</v-icon></v-btn>
        <v-btn fab depressed small dark color="pink" class="hidden-md-and-up hidden-sm-only" ><v-icon >mdi-heart</v-icon></v-btn>
```
### Toolbars

### Navigation Drawers

### Adding Routes
```

```
###  Padding & Margin
- yarn add material-design-icons-iconfont -D
- npm install date-fns

###  Firebase & Firestore Databases
- Firebase provides a back-end as a service to our apps
   - Data storage, authentication, hosting, storage etc
     - front-end (browser vue) 
     - Back-end (server firestore)
 - One of the service provided by Firebase
 - A NoSQL database to store data in
 - Contains `collections` of `documents`
 - Allows us to update our app in real-time
###### Example
| Firestore Database | Projects Collection | Single Document |
| ------ | ------ |------ |
| users |ABC123DEF456  | { title: 'make site' |
| projects |JKH378SVF213  |due: '7th August 2019' |
| chats | LXM346HQI712 |person: 'Idrees' |
| cafes |PQC891DOK119 |status: 'ongoing' |
| ratings |LXM346HQI712 | } |

#### Resources
- https://vuetifyjs.com/en/styles/colors
- https://vuetifyjs.com/en/styles/typography#font-sizes
- https://vuetifyjs.com/en/components/buttons
- https://vuetifyjs.com/en/components/icons
- https://vuetifyjs.com/en/customization/breakpoints#breakpoints
- https://vuetifyjs.com/en/styles/display#visibility
- https://vuetifyjs.com/en/components/toolbars
- https://vuetifyjs.com/en/components/navigation-drawers#examples
- https://vuetifyjs.com/en/components/lists#lists
- https://vuetifyjs.com/en/styles/spacing#negative-margin
- https://vuetifyjs.com/en/components/app-bars
- https://vuetifyjs.com/en/components/grids#grid-system
- https://vuetifyjs.com/en/components/chips#chips
- https://vuetifyjs.com/en/components/tooltips#tooltips
- https://vuetifyjs.com/en/components/cards#cards
- https://vuetifyjs.com/en/components/avatars#avatars
- https://vuetifyjs.com/en/components/expansion-panels#expansion-panels
- https://vuetifyjs.com/en/components/menus#menus
- https://vuetifyjs.com/en/components/dialogs#dialogs