/*new  Vue({
  el:"#vue-app",
  data:{
  name:"John",
  job:"ABc",
  age:'',
  website:'http://google.com',
  websiteTag:'<a href="http://google.com">Google.com</a>'
},
methods:{
  greet:function(time){
    return "Good "+ time +' '+ this.name;
  },
  logName:function(){
    //console.log("you enter your name");
  },
  logAge:function(){
    console.log("you enter your name");
  }
}

}); */
/*
new  Vue({
  el:"#vue-event",
  data:{
    age:24,
    x:0,
    y:0,
    
   
  },
  methods:{
    add:function(inc){
      this.age+=inc;
    },
    subtract:function(dec){
      this.age-=dec;
    },
    updateXY:function(event){
      console.log(event);
    this.x = event.offsetX;
    this.y = event.offsetY;
    },
    click:function(){
      alert("You click me");
    },
    logName:function(){
      console.log("you enter your name");
    },
    logAge:function(){
      console.log("you enter your name");
    },
  }
});

new  Vue({
  el:"#vue-computed",
  data:{
    age:24,
    a:0,
    b:0
    
   
  },
  methods:{
    // addToA:function(){
    //   return this.a + this.age;
    // },
    addToB:function(){
       return this.b + this.age;
     }
  },
  computed:{
    addToA:function(){
      console.log('addToA');
      return this.a + this.age;
    }
  }
});*/

/*new  Vue({
  el:"#vue-dyncss",
  data:{
   available:false,
   nearby:false
    
   
  },
  methods:{
  
  },
  computed:{
  compClasses:function(){
    return{
      available: this.available,
      nearby: this.nearby,
    }
  }
  }
});*/

/*new  Vue({
  el:"#vue-conditionals",
  data:{
   error:false,
   success:false
  },
  methods:{
  
  },
  computed:{
  
  }
});*/

/*new  Vue({
  el:"#vue-looping",
  data:{
   characters:['Mario','Luigi','Yoshi','Bowser'],
   ninjas:[
   {name:'Ryu',age:25},
   {name:'Yoshi',age:35},
   {name:'Ken',age:55},
   ]
  },
  methods:{
  
  },
  computed:{
  
  }
});*/
/*new  Vue({
  el:"#vue-punchbag",
  data:{
  health:100,
  ended:false
  },
  methods:{
    punch:function(){
      this.health -= 10;
      if(this.health <= 0){
        this.ended=true;
      }
    },
    restart:function(){
      this.health = 100;
      this.ended = false;
    }

  },
  computed:{
  
  }
});*/
/*var one = new  Vue({
  el:"#app-one",
  data:{
  title:'Vue App One',
  },
  methods:{
    greet:function(){
      return 'Hello from app one';
    }

  },
  computed:{
  
  }
});
var two = new  Vue({
  el:"#app-two",
  data:{
  title:'Vue App Two',
  },
  methods:{
    changeTitle:function(){
      one.title = 'Title Changed';
    }
  },
  computed:{
  greet:function(){
      return 'Hello from app two';
    }
  }
});

two.title = 'Changed from Outside';*/
/*Vue.component('greeting',{
template:'<p>Hi,there, I am {{name}}.<button v-on:click="changeName">change Name</button></p>',
data:function(){
  return{
    name:'Yoshi'
}
},
methods:{
  changeName:function(){
    this.name ='Mario';
  }
}


});
var one = new  Vue({
  el:"#app-one",
  data:{
  
  },
  methods:{
 

  },
  computed:{
  
  }
});
var two = new  Vue({
  el:"#app-two",
  data:{
  
  },
  methods:{
    
  },
  computed:{
  
  }
});*/

new  Vue({
  el:"#app-ref",
  data:{
  output:'Your fav Food'
  },
  methods:{
 readRefs:function(){
  //console.log(this.$refs.input.value);
  console.log(this.$refs);
  this.output= this.$refs.test.innerText;
 }

  },
  computed:{
  
  }
});