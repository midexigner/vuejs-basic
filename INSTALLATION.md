# Nuxt Framework
- npx create-nuxt-app projectName
- import Logo from '~/components/Logo'
- 

# apex Legend API
- npm init
- create package.json
```sh
{
  "name": "apex-legend",
  "version": "1.0.0",
  "description": "Track stats for Apex Legends",
  "main": "server.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Muhammad Idrees",
  "license": "MIT"
}

```
- npm i express dotenv morgan node-fetch concurrently
- npm i -D nodemon
- open package.json then modifile files

```sh
"scripts":{
	"start":"node server",
	"server":"nodemon server"
}
```
-  create `server.js`

```sh
 const express = require('express');
 const morgan = require('morgan');
 const dotenv = require('dotenv');
const port = 5000;

 const app = express();

 app.listen(port,()=>{
 console.log(`Example app listening on port ${port}!`)
 });
```
 - then type `npm run server`
 - then create `config.env` file
```sh
PORT=5000
NODE_ENV=development
TRACKER_API_URL=https://public-api.tracker.gg/v2/apex/standard
TRACKER_API_KEY=0a597f37-6edc-4862-a05f-ca5822609e98

```
- then open server.js

```sh
// Load env
dotenv.config({path:'./config.env'});
const port = process.env.PORT || 8000;


app.listen(port, () => {
    console.log(`Server Running in ${process.env.NODE_ENV} on port ${port}!`)
});
```

- create a `routes` folder and create a `profile.js` and paste below code

```sh
const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');

router.get('/:platform/:gamertag', async(req, res) => {
    try {
        const headers = {
            'TRN-Api-Key': process.env.TRACKER_API_KEY
        }
        const { platform, gamertag } = req.params;

        const response = await fetch(`${process.env.TRACKER_API_URL}/profile/${platform}/${gamertag}`, {
            headers
        });
        const data = await response.json();
        if (data.errors && data.errors.length > 0) {
            return res.status(404).json({
                message: "Profile Not  Found"
            });
        }
        res.json(data);
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: "Server Error"
        });
    }

});

module.exports = router;
```
- then open server.js

```sh
// profile routes
app.use('/api/v1/profile', require('./routes/profile'));

```

- then `morgan package` configure in server.js

```sh
// Dev logging
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}
```
- then vue install globally  `npm i -g @vue/cli` 
- then `vue create client`
- then modify package.json root file open
```sh
"scripts":{
	"start":"node server",
	"server":"nodemon server",
	"client":"npm run serve --prefix client",
	"dev":"concurrently \"npm run server\" \"npm run client\""
}
```
- then type `npm run dev `
- then vue router install `npm i vue-router axios vue-toasted`