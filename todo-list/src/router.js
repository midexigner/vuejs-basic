import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/Dashboard'
import Pages from './views/Pages'
import Projects from './views/Projects'
import About from './views/About'
import Account from './views/Account'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
     component: Dashboard
      
    },
    {
      path: '/pages',
      name: 'pages',
      component: Pages
    },
    {
      path: '/projects',
      name: 'projects',
      component: Projects
    },
    {
      path: '/account',
      name: 'account',
      component: Account
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/contact',
      name: 'contact',
      component: () => import('./views/Contact.vue')
    }
  ]
})
