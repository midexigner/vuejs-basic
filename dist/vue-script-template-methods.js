var app = new Vue({
			//el:"#app",
			template:`<div id='wrapper'>
			<h1>{{message}}</h1>
		<button @click="showHide">Click me</button>
			</div>`,
	data:{
		message: "Hello world!"
	},
	methods:{
		showHide(){
			console.log("Show me hide method called.")
		},
		showHides:function(){
			console.log("Show me hides method called 2+2.")
		}
		
	},
	mounted(){
		// onload view call function
			this.showHides();
		}


}).$mount('#app');