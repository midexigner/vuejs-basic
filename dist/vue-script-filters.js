var app = new Vue({
data:{
	message:"hello from Vue Js"
},
filters:{
	upperCase:function(value){
		return value.toUpperCase();
	},
	lowerCase:function(value){
		return value.toLowerCase();
	},
	capitalize:function(value){
		var val = value.toString();
		return val.charAt(0).toUpperCase()+val.slice(1);
	}
}

}).$mount('#app');
